package com.example.shuvagin_l27_retrofit.ui.main.domain.adapter

import androidx.core.graphics.drawable.RoundedBitmapDrawable
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.Todos
import com.example.shuvagin_l27_retrofit.ui.main.domain.model.TodosColor
import com.example.shuvagin_l27_retrofit.ui.main.domain.model.TodosColorImpl

// The main task of adapters is to convert the entities used by the database
// and network clients to Domain module entities and back. This conversion has both pros and cons
// - Any data changes in one layer don’t affect other layers
// - Third-party dependencies as well as annotations required for a library don’t fall into other layers
// - There’s a possibility of multiple duplications
// - While changing the data you have to change the mapper

object TodosAdapter {

    fun toStorage(todosColor: TodosColor) = Todos(
        todosColor.completed, todosColor.id, todosColor.title, todosColor.userId
    )

    fun fromStorage(todos: Todos, icon: RoundedBitmapDrawable) = TodosColorImpl(
        todos.completed, todos.id, todos.title, todos.userId, icon
    )
}
