package com.example.shuvagin_l27_retrofit.ui.aboutUser.domain.usecase

import com.example.shuvagin_l27_retrofit.data.sourceTodos.TodosRepository
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.User
import com.example.shuvagin_l27_retrofit.di.TodosRepositoryModule
import com.example.shuvagin_l27_retrofit.ui.base.BaseUseCase
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class GetUserInfoByIdUseCase @Inject constructor(
    @TodosRepositoryModule.TodoRemoteRepository private val todosRepository: TodosRepository
) : BaseUseCase() {

    fun execute(userId: Int, listener: (Result<User>) -> Unit) {
        todosRepository.getUser(userId).subscribe(object : SingleObserver<User?> {
            override fun onSuccess(user: User) {
                listener.invoke(Result.success(user))
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onError(e: Throwable) {
                listener.invoke(Result.failure(e))
            }
        })
    }
}
