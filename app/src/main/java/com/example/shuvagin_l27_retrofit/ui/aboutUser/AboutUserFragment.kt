package com.example.shuvagin_l27_retrofit.ui.aboutUser

import android.content.Context
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.transition.doOnEnd
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.shuvagin_l27_retrofit.databinding.FragmentAboutUserBinding
import com.example.shuvagin_l27_retrofit.di.injector
import com.example.shuvagin_l27_retrofit.utils.viewModel

/**
 * A simple [Fragment] subclass.
 */
class AboutUserFragment : Fragment() {

    private lateinit var binding: FragmentAboutUserBinding
    private val args: AboutUserFragmentArgs by navArgs()
    private val viewModel by viewModel {
        injector.mainViewModel
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val enterTransitionAnimation = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        enterTransitionAnimation.doOnEnd { showEnterAnimation() }
        sharedElementEnterTransition = enterTransitionAnimation

        setOnBackPressedAction()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAboutUserBinding.inflate(inflater, container, false).apply {
            args = this@AboutUserFragment.args
            viewmodel = viewModel
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadUser(args.userId)
    }

    private fun showEnterAnimation() {
        binding.tvUserId.animate().scaleX(MAX_SCALE_SIZE).scaleY(MAX_SCALE_SIZE).start()
    }

    private fun setOnBackPressedAction() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            binding.tvUserId.animate().scaleX(NORMAL_SCALE_SIZE).scaleY(NORMAL_SCALE_SIZE).setDuration(ANIMATION_EXIT_DURATION).withEndAction { findNavController().popBackStack() }.start()
        }
    }

    companion object {
        const val MAX_SCALE_SIZE = 2f
        const val NORMAL_SCALE_SIZE = 1f
        const val ANIMATION_EXIT_DURATION = 100L
    }
}
