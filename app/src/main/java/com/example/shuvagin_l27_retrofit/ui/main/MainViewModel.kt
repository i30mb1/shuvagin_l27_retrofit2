package com.example.shuvagin_l27_retrofit.ui.main

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.User
import com.example.shuvagin_l27_retrofit.ui.aboutUser.domain.usecase.GetUserInfoByIdUseCase
import com.example.shuvagin_l27_retrofit.ui.base.BaseViewModel
import com.example.shuvagin_l27_retrofit.ui.main.domain.model.TodosColor
import com.example.shuvagin_l27_retrofit.ui.main.domain.usecase.GetTodosColorUseCase
import javax.inject.Inject

class MainViewModel @Inject constructor(
    application: Application,
    private val getTodosColorUseCase: GetTodosColorUseCase,
    private val getUserByIdUseCase: GetUserInfoByIdUseCase
) : BaseViewModel(application) {

    private val _listTodos = MutableLiveData<List<TodosColor>>()
    val listTodos: LiveData<List<TodosColor>> = _listTodos
    private val _isLoading = MutableLiveData<Boolean>(false)
    val isLoading: LiveData<Boolean> = _isLoading
    private val _selectedUser = MutableLiveData<User>()
    val selectedUser: LiveData<User> = _selectedUser

    init {
        loadTodos()
    }

    private fun loadTodos() {
        _isLoading.value = true
        getTodosColorUseCase.execute { result: Result<List<TodosColor>> ->
            result.onSuccess {
                _listTodos.value = it
            }
            _isLoading.value = false
        }
    }

    fun loadUser(userId: Int) {
        _isLoading.value = true
        getUserByIdUseCase.execute(userId) { result: Result<User> ->
            result.onSuccess {
                _selectedUser.value = it
            }
            _isLoading.value = false
        }
    }
}
