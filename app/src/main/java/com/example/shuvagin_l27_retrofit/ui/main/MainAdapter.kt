package com.example.shuvagin_l27_retrofit.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.shuvagin_l27_retrofit.databinding.ItemTodoBinding
import com.example.shuvagin_l27_retrofit.ui.main.domain.model.TodosColor
import com.example.shuvagin_l27_retrofit.ui.main.domain.model.TodosColorImpl

class MainTodosAdapter : ListAdapter<TodosColor, MainTodosAdapter.ViewHolder>(MainDiffCallback()) {

    class ViewHolder private constructor(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(todos: TodosColor) {
            val itemTodoBinding = binding as ItemTodoBinding
            itemTodoBinding.todosColor = todos as TodosColorImpl
            itemTodoBinding.viewHolder = this
            itemTodoBinding.root.setOnClickListener {
                navigateToUser(todos, itemTodoBinding)
            }
            itemTodoBinding.executePendingBindings()
        }

        private fun navigateToUser(todos: TodosColor, binding: ItemTodoBinding) {
            val directions = MainFragmentDirections.actionMainFragmentToFullTodos(todos.userId, binding.tvUserId.transitionName)
            val extras = FragmentNavigatorExtras(binding.tvUserId to binding.tvUserId.transitionName)
            binding.root.findNavController().navigate(directions, extras)
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemTodoBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val todos = getItem(position)
        holder.bind(todos)
    }
}

class MainDiffCallback : DiffUtil.ItemCallback<TodosColor>() {
    override fun areItemsTheSame(oldItem: TodosColor, newItem: TodosColor): Boolean {
        return oldItem.userId == newItem.userId
    }

    override fun areContentsTheSame(oldItem: TodosColor, newItem: TodosColor): Boolean {
        return oldItem.title == newItem.title
    }
}
