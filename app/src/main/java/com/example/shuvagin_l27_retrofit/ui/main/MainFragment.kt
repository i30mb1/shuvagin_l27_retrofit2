package com.example.shuvagin_l27_retrofit.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.shuvagin_l27_retrofit.databinding.MainFragmentBinding
import com.example.shuvagin_l27_retrofit.di.injector
import com.example.shuvagin_l27_retrofit.utils.viewModel

class MainFragment : Fragment() {

    lateinit var mainTodosAdapter: MainTodosAdapter
    lateinit var binding: MainFragmentBinding
    private val mainViewModel: MainViewModel by viewModel {
        injector.mainViewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = mainViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupAdapter()
    }

    private fun setupAdapter() {
        mainTodosAdapter = MainTodosAdapter()
        binding.rvMainFragment.apply {
            adapter = mainTodosAdapter
            // return animation with this work fine
            postponeEnterTransition()
            viewTreeObserver.addOnPreDrawListener { startPostponedEnterTransition(); true }
        }
        mainViewModel.listTodos.observe(viewLifecycleOwner, Observer {
            mainTodosAdapter.submitList(it)
        })
    }
}
