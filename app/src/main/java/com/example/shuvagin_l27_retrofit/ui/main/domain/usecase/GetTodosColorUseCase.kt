package com.example.shuvagin_l27_retrofit.ui.main.domain.usecase

import android.app.Application
import android.graphics.drawable.ColorDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.graphics.drawable.toBitmap
import com.example.shuvagin_l27_retrofit.data.sourceTodos.TodosRepository
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.Todos
import com.example.shuvagin_l27_retrofit.di.TodosRepositoryModule.TodoRemoteRepository
import com.example.shuvagin_l27_retrofit.ui.base.BaseUseCase
import com.example.shuvagin_l27_retrofit.ui.main.domain.adapter.TodosAdapter
import com.example.shuvagin_l27_retrofit.ui.main.domain.model.TodosColor
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class GetTodosColorUseCase @Inject constructor(
    @TodoRemoteRepository private val todosRepository: TodosRepository,
    private val application: Application
) : BaseUseCase() {

    private val colorList = listOf(
        0xfff44336,
        0xffe91e63,
        0xff9c27b0,
        0xff673ab7,
        0xff3f51b5,
        0xff2196f3,
        0xff03a9f4,
        0xff00bcd4,
        0xff009688,
        0xff4caf50,
        0xff8bc34a,
        0xffcddc39,
        0xffffeb3b,
        0xffffc107,
        0xffff9800,
        0xffff5722,
        0xff795548,
        0xff9e9e9e,
        0xff607d8b,
        0xff333333
    )

    fun execute(listener: (Result<List<TodosColor>>) -> Unit) {
        todosRepository.getTodos().map { list: List<Todos> ->
            transformToTodosColor(list)
        }.subscribe(object : SingleObserver<ArrayList<TodosColor>?> {
            override fun onSuccess(list: ArrayList<TodosColor>) {
                listener.invoke(Result.success(list))
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onError(e: Throwable) {
                listener.invoke(Result.failure(e))
            }
        })
    }

    private fun transformToTodosColor(list: List<Todos>): ArrayList<TodosColor> {
        val newList = ArrayList<TodosColor>()
        list.forEach {
            val icon = generateRandomIcon(it)
            val todosColor = TodosAdapter.fromStorage(it, icon)
            newList.add(todosColor)
        }
        return newList
    }

    private fun generateRandomIcon(it: Todos): RoundedBitmapDrawable {
        val color = colorList[it.id % colorList.size].toInt()
        val drawable = ColorDrawable(color)
        val icon = RoundedBitmapDrawableFactory.create(
            application.resources, drawable.toBitmap(WIDTH, HEIGHT)
        )
        icon.isCircular = true
        return icon
    }

    companion object {
        const val WIDTH = 100
        const val HEIGHT = WIDTH
    }
}
