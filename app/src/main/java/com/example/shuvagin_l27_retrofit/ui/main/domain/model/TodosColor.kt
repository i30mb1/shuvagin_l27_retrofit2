package com.example.shuvagin_l27_retrofit.ui.main.domain.model

import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.databinding.ObservableBoolean

interface TodosColor {
    var completed: Boolean
    val id: Int
    val title: String
    val userId: Int
    var icon: RoundedBitmapDrawable
    val liveDataCompleted: ObservableBoolean
    val tnUserId: String
}
