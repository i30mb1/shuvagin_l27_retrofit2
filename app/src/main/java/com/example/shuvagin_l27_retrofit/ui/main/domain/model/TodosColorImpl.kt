package com.example.shuvagin_l27_retrofit.ui.main.domain.model

import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.databinding.ObservableBoolean

class TodosColorImpl(
    override var completed: Boolean = false,
    override val id: Int = 0,
    override val title: String = "",
    override val userId: Int = 0,
    override var icon: RoundedBitmapDrawable
) : TodosColor {

    override val liveDataCompleted = ObservableBoolean(completed)
    override val tnUserId = "tnUserId_$id"
}
