package com.example.shuvagin_l27_retrofit.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel

abstract class BaseViewModel(application: Application, vararg useCases: BaseUseCase) :
    AndroidViewModel(application) {

    protected var useCaseList: MutableList<BaseUseCase> = mutableListOf()

    init {
        useCaseList.addAll(useCases)
    }

    override fun onCleared() {
        super.onCleared()
        useCaseList.forEach { it.dispose() }
    }
}
