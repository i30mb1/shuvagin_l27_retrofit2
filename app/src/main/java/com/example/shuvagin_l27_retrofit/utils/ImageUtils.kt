package com.example.shuvagin_l27_retrofit.utils

import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.databinding.BindingAdapter
import coil.api.load

@BindingAdapter("loadImageUrl")
fun ImageView.loadImageUrl(icon: RoundedBitmapDrawable) {
    this.load(icon)
}
