package com.example.shuvagin_l27_retrofit.di

import com.example.shuvagin_l27_retrofit.data.sourceTodos.TodosRepository
import com.example.shuvagin_l27_retrofit.data.sourceTodos.local.TodosLocalRepository
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.TodosApi
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.TodosRemoteRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Qualifier

@Module
object TodosRepositoryModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class TodoLocalRepository

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class TodoRemoteRepository

    @TodoLocalRepository
    @Provides
    @Reusable
    fun getTodosLocalRepo(api: TodosApi): TodosRepository {
        return TodosLocalRepository()
    }

    @TodoRemoteRepository
    @Provides
    @Reusable
    fun getTodosRemoteRepo(api: TodosApi): TodosRepository {
        return TodosRemoteRepository(api)
    }
}
