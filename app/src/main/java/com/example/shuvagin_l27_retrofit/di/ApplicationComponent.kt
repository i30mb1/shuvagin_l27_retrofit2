package com.example.shuvagin_l27_retrofit.di

import android.app.Application
import com.example.shuvagin_l27_retrofit.ui.main.MainViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class, TodosRepositoryModule::class])
interface ApplicationComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Application): ApplicationComponent
    }

//    @Singleton
//    чтобы класс стал синглтоном нужно пометить сам класс этой аннотацией
    val mainViewModel: MainViewModel
}
