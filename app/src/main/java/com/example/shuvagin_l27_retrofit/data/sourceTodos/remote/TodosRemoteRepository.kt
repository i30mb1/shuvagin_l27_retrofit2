package com.example.shuvagin_l27_retrofit.data.sourceTodos.remote

import com.example.shuvagin_l27_retrofit.data.sourceTodos.TodosRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TodosRemoteRepository @Inject constructor(private val todosApi: TodosApi) : TodosRepository {

    override fun getTodos(): Single<List<Todos>> =
        todosApi.getTodos().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    override fun getUser(userId: Int): Single<User> =
        todosApi.getUsers(userId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}
