package com.example.shuvagin_l27_retrofit.data.sourceTodos.local

import com.example.shuvagin_l27_retrofit.data.sourceTodos.TodosRepository
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.Todos
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.User
import io.reactivex.Single
import javax.inject.Inject

class TodosLocalRepository @Inject constructor() : TodosRepository {
    override fun getTodos(): Single<List<Todos>> {
        return getTodos()
    }

    override fun getUser(userId: Int): Single<User> {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }
}
