package com.example.shuvagin_l27_retrofit.data.sourceTodos.remote
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Todos(
    @Json(name = "completed")
    var completed: Boolean = false,
    @Json(name = "id")
    val id: Int = 0,
    @Json(name = "title")
    val title: String = "",
    @Json(name = "userId")
    val userId: Int = 0
)
