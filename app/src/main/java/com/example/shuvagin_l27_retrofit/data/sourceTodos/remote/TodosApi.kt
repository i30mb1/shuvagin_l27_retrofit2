package com.example.shuvagin_l27_retrofit.data.sourceTodos.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface TodosApi {

    @GET("todos/")
    fun getTodos(): Single<List<Todos>>

    @GET("users/{id}")
    fun getUsers(@Path("id") userId: Int): Single<User>
}
