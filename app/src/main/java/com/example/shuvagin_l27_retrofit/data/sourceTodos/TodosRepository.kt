package com.example.shuvagin_l27_retrofit.data.sourceTodos

import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.Todos
import com.example.shuvagin_l27_retrofit.data.sourceTodos.remote.User
import io.reactivex.Single

interface TodosRepository {

    fun getTodos(): Single<List<Todos>>

    fun getUser(userId: Int): Single<User>
}
